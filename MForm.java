import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;



public class MForm extends JFrame implements ActionListener  {
    protected MForm frame;

    private JPanel mPanel;
    private JPanel btnPanel;
    private JPanel logPanel;
    private JPanel botPanel;
    private JPanel tfieldPanel;


    private JButton bExit;
    private JButton bRefresh;
    private JButton bAdd;
    private JButton bDelete;
    private JButton bModify;

    private JLabel log;
    private JLabel help;
    private Postgresql pgsql;
    private JTable table;
    private JTextField tfName;
    private JTextField tfTel;
    private JScrollPane scrollPane;

    private void createForm() {
        MForm frame = new MForm();
        frame.setName("Contacts");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 600);

        tfName = new JTextField();
        tfTel = new JTextField();

        log = new JLabel("Hello!");
        help = new JLabel("Enter name and number, then press Add or Modify");

        tfieldPanel = new JPanel();
        mPanel = new JPanel();
        botPanel = new JPanel();
        btnPanel = new JPanel();
        logPanel = new JPanel();

        bExit = new JButton("Exit");
        bRefresh = new JButton("Refresh");
        bAdd = new JButton("Add");
        bDelete = new JButton("Delete");
        bModify = new JButton("Modify");

        bExit.addActionListener(this);
        bRefresh.addActionListener(this);
        bDelete.addActionListener(this);
        bAdd.addActionListener(this);
        bModify.addActionListener(this);

        tfieldPanel.setLayout(new BorderLayout());
        btnPanel.setLayout(new BorderLayout());
        logPanel.setLayout(new BorderLayout());

        DefaultTableModel model = new DefaultTableModel();
        table = new JTable(model);
        pgsql = new Postgresql();
        table = pgsql.createTable();
        scrollPane = new JScrollPane(table);


        tfieldPanel.add(BorderLayout.NORTH,tfName);
        tfieldPanel.add(BorderLayout.SOUTH,tfTel);
        tfieldPanel.add(BorderLayout.CENTER,help);
        mPanel.add(scrollPane);
        logPanel.add(BorderLayout.WEST, log);
        botPanel.add(bAdd);
        botPanel.add(bModify);
        botPanel.add(bDelete);
        botPanel.add(bRefresh);
        botPanel.add(bExit);
        btnPanel.add(BorderLayout.NORTH, botPanel);
        btnPanel.add(BorderLayout.CENTER,tfieldPanel);

        //tfieldPanel.setBackground(Color.red);

        frame.getContentPane().add(BorderLayout.CENTER, btnPanel);
        frame.getContentPane().add(BorderLayout.SOUTH, logPanel);
        frame.getContentPane().add(BorderLayout.NORTH, mPanel);

        //frame.remove(table);
        frame.setVisible(true);
        //return frame;
    }


    public static void main(String[] args) {
     MForm MFrame = new MForm();
     MFrame.createForm();



    }

    public void actionPerformed(ActionEvent e) {
      if(e.getActionCommand()=="Exit") System.exit(0);

        Postgresql pg = new Postgresql();
        try {
            Class.forName("org.postgresql.Driver");
            pg.con = DriverManager.getConnection("jdbc:postgresql://shspu.ru:35102/Contacts", "postgres", "123");
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

      if(e.getActionCommand()=="Refresh") {
          try {
              pg.stmt = pg.con.createStatement();
              pg.rs = pg.stmt.executeQuery("select * from person where status = TRUE order by name;");
              pg.counter = 1;
              ((DefaultTableModel)table.getModel()).setRowCount(0);
              while (pg.rs.next()) {
                  ((DefaultTableModel)table.getModel()).addRow(new Object[]{pg.counter, pg.rs.getString(2),pg.rs.getString(3)});
                  pg.counter++;
              }
              ((DefaultTableModel)table.getModel()).fireTableDataChanged();
          }catch (Exception ex) {
              System.out.println(ex.getMessage());
          }
          log.setText("Table refreshed successful");
      }

      else if (e.getActionCommand()=="Delete"){
          if(table.getSelectedRow()>=0) {
              String name, tel;
              int rownum = table.getSelectedRow();
              name = (String) table.getValueAt(rownum,1);
              tel = (String) table.getValueAt(rownum,2);

              try {
                  pg.stmt = pg.con.createStatement();
                  pg.stmt.executeUpdate("update person set status=FALSE where name='" + name + "' and tel='"+ tel +"'");
                  ((DefaultTableModel)table.getModel()).removeRow(rownum);
              }catch (Exception ex){
                  System.out.println(ex.getMessage());
              }
          } else {
              log.setText("Need to select a row");
          }
      }
        else if (e.getActionCommand()=="Add") {
            if((tfName.getText().length()!=0)&&(tfTel.getText().length()!=0)) {
                String name, tel;
                name = tfName.getText();
                tel = tfTel.getText();
                try {
                    pg.stmt = pg.con.createStatement();

                    if (pg.stmt.executeUpdate("update person set status=TRUE where name='" + name + "' and tel='" + tel + "'") == 0) {

                        if (!(pg.stmt.executeQuery("select * from person where tel='" + tel + "'").next())) {
                            pg.rs = pg.stmt.executeQuery("select max (id) from person");
                            pg.rs.next();
                            int id = pg.rs.getInt(1) + 1;
                            pg.stmt.executeUpdate("insert into person values(" + id + ",'" + name + "','" + tel + "',TRUE)");
                            log.setText("Please, refresh the table");
                            tfName.setText("");
                            tfTel.setText("");
                        } else log.setText("This number already exists");
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }else log.setText("Enter name and number");
          }

           else if (e.getActionCommand()=="Modify"){
              if (table.getSelectedRow() >= 0) {
                  String name, tel;
                  int rownum = table.getSelectedRow();
                  name = (String) table.getValueAt(rownum, 1);
                  tel = (String) table.getValueAt(rownum, 2);
                  System.out.println(tfName.getColumns());


                  if(tfName.getText().length()==0)tfName.setText(name);
                  if(tfTel.getText().length()==0)tfTel.setText(tel);

                  try {
                      pg.stmt = pg.con.createStatement();
                      pg.stmt.executeUpdate("update person set status=TRUE, name='"+tfName.getText()+"', tel='"+tfTel.getText()+"' where name='" + name + "' and tel='" + tel + "'");
                      log.setText("Please, refresh the table");
                      tfName.setText("");
                      tfTel.setText("");
                  } catch (Exception ex) {
                      System.out.println(ex.getMessage());
                  }
              } else {
                  log.setText("Need to select a row");
              }

      }
      }


}