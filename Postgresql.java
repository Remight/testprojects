import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;

public class Postgresql {
    protected Connection con;
    protected Statement stmt;
    protected ResultSet rs;
    protected int counter;


   protected JTable table;

    public JTable createTable() {


        DefaultTableModel model = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        table = new JTable(model);
        model.addColumn("№");
        model.addColumn("Name");
        model.addColumn("Telephone");
        counter = 1;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://shspu.ru:35102/Contacts","postgres","123");
            stmt = con.createStatement();
            rs = stmt.executeQuery("select * from person where status = TRUE order by name;");
            while (rs.next()) {
                model.addRow(new Object[]{counter, rs.getString(2),rs.getString(3)});
                counter++;

            }
            rs.close();
            stmt.close();
            con.close();
        } catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        return table;
    }




}
